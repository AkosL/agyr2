#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUFFLEN 512
#define NPACK 10
#define PORT 15000

void diep(char *p){
  perror(p);
  exit(1);
}

int main(void){
  struct sockaddr_in si_me, si_other;
  int s, i, slen=sizeof(si_other);
  char buff[BUFFLEN];
  char *cp;
  
  if((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){ diep("socket"); }
  
  memset((char*) &si_me, 0, sizeof(si_me));
  
  si_me.sin_family = AF_INET;
  si_me.sin_port = htons(PORT);  
  si_me.sin_addr.s_addr = htonl(INADDR_ANY);
  
  if(bind(s, (struct sockaddr *)&si_me, sizeof(si_me)) == -1){ diep("bind"); }
  
  while(1){
    if(recvfrom(s, buff, BUFFLEN, 0, (struct sockaddr *)&si_other, &slen) == -1){ diep("recvfrom()"); }
    
    cp = strchr(buff, '\n');
    *cp = 0;
    printf("%s\n", buff);
  }
  
  close(s);
  
  return 0;
}
