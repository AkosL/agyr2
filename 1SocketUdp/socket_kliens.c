#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUFLEN 512
#define NPACK 10
#define PORT 15000
#define SRV_IP "127.0.0.1"
//#define SRV_IP "10.34.43.87"

void diep(char *p){
  perror(p);
  exit(1);
}

int main(void){
  struct sockaddr_in si_other;
  int i, s, slen=sizeof(si_other);
  char buf[BUFLEN];
  char *cp;
  char c;

  if((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1){ diep("socket"); }
  
  memset((char*) &si_other, 0, sizeof(si_other));
  
  si_other.sin_family = AF_INET;
  si_other.sin_port = htons(PORT);

  printf("x\n");
    
  if(inet_aton(SRV_IP, &si_other.sin_addr)==0){ diep("inet_aton()"); }

  printf("y\n");
  sprintf(buf, "Hello!\n");
   if(sendto(s, buf, strlen(buf), 0, (struct sockaddr *)&si_other, slen) == -1){ 
      diep("sendto()"); 
    }
  
  while(1){
    i = 0;
    do{ c=getc(stdin); buf[i++]=c; } while(c!=10 && i<BUFLEN);

    buf[i]=0;

		strcat(buf,"\n");
	
		printf(" %d karakter\n", i);
		if (sendto(s, buf, strlen(buf), 0, (struct sockaddr*)&si_other, slen)==-1){
		  diep("sendto()");
	  }
		usleep(1);
		  
/*  
    for(i = 1; i < NPACK; i++){
    printf("sending packet %d\n", i);
    sprintf(buf, "Hello\n");
    if(sendto(s, buf, strlen(buf), 0, (struct sockaddr *)&si_other, slen) == -1){ 
      diep("sendto()"); 
    }
    usleep(1);
*/
  }
  
  
  close(s);
  
  return 0;
}
