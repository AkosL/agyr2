/* tcp server */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <arpa/inet.h>

const int PORT = 15000;

int main(void) {
  int cont, create_socket, new_socket, addrlen;
  int bufsize = 1024;
  
  char *buffer = (char *)malloc(bufsize);
  struct sockaddr_in address;

  char *inetadd;
  
  if ((create_socket = socket(AF_INET,SOCK_STREAM,0)) > 0) {
    printf("The socket was created\n");
  }
  
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);
  
  if (bind(create_socket,(struct sockaddr *)&address,sizeof(address)) == 0) {
    printf("Binding socket @ PORT%d\n", PORT);
  }
  
  listen(create_socket,3);
  
  addrlen = sizeof(struct sockaddr_in);
  new_socket = accept(create_socket,(struct sockaddr *)&address,&addrlen);
  
  if (new_socket > 0) {
    inetadd = inet_ntoa(address.sin_addr);
    printf("The client %s is connected...\n", inetadd);
    
    for(cont = 1; cont < 5000; cont++) {
      printf("\x7");
    }
  }
  do {
    printf("Message to send: ");
    scanf("%s", buffer);
    send(new_socket,buffer,bufsize,0);
    recv(new_socket,buffer,bufsize,0);
    printf("Message recieved: %s\n",buffer);
    
  } while (strncmp(buffer,"/q",2));
  
  close(new_socket);
  close(create_socket);
  return 0;
}






