/* mutex_20141107 
	mutuális exkludálás

	ez itt nincs kész!!!!

	fordításhoz külön be kell húzni a pthread-et: -lpthread
	
	{
	"cmd" : ["gcc", "$file_name", "-o", "${file_base_name}", "-std=c99", "-lpthread", "-lm", "-Wall"],
	"selector" : "source.c",
	"shell":false,
	"working_dir" : "$file_path"
	}
*/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

#define WORK_AREA_SIZE 1024

void *thread_func(void *arg);
char work_area[WORK_AREA_SIZE];
pthread_mutex_t work_mutex;

int time_to_exit = 0;

int main(void) {
	int res;
	pthread_t myThread;
	void* threadResult;

	
	

	pthread_mutex_create(&work_mutex, NULL);
	pthread_create(&myThread, NULL, thread_func, NULL);
	
	pthread_mutex_lock(&work_mutex);
	printf("írjá be vlamit, és csapj egy entert!\n");

	while(!time_to_exit){
		fgets(work_area, WORK_AREA_SIZE, stdin);
		pthread_mutex_unlock(&work_mutex);

		while(1){
			pthread_mutex_lock(&work_mutex);
			if(work_area[0] != '\0'){
				pthread_mutex_unlock(&work_mutex);
				sleep(1);
			}else{
				break;
			}
		}
	}
	pthread_mutex_unlock(&work_mutex);
	//ez itt nincs kész!!!!

	printf("Main thread stops\n");
	return 0;
}

void *thread_funcA(void* arg){
	sleep(1);

	printf("Thread A starts...\n");

	sem_wait(&bin_sem);

	while(strncmp("end", work_area, 3) != 0){
		printf("you input %d characters\n", strlen(work_area));
		work_area[0] = 0;
		sem_wait(&bin_sem);
	}

	printf("Thread A stops...\n");

	return NULL;
}