/* thread2_20141107 
	futtassunk két szálat...

	fordításhoz külön be kell húzni a pthread-et: -lpthread
	
	{
	"cmd" : ["gcc", "$file_name", "-o", "${file_base_name}", "-std=c99", "-lpthread", "-lm", "-Wall"],
	"selector" : "source.c",
	"shell":false,
	"working_dir" : "$file_path"
	}
*/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

void *thread_funcA(void *arg);
void *thread_funcB(void *arg);

int main(void) {

	pthread_t myThreadA, myThreadB;

//indíjjuk el a threadeket
	if(pthread_create(&myThreadA, NULL, thread_funcB, NULL)){
		fprintf(stderr, "BUGZIK A!\n");
		exit(1);
	}

	if(pthread_create(&myThreadB, NULL, thread_funcA, NULL)){
		fprintf(stderr, "BUGZIK B!\n");
		exit(1);
	}


//és várjuk meg őket
	if(pthread_join(myThreadA, NULL)){
		fprintf(stderr, "Bugzik a join A\n");
		exit(1);
	}

	if(pthread_join(myThreadB, NULL)){
		fprintf(stderr, "Bugzik a join B\n");
		exit(1);
	}


	printf("Main thread stops\n");
	return 0;
}

void *thread_funcA(void* arg){

	printf("Thread A starts...\n");

	for (int i = 0; i < 10; ++i){
		printf("Th A %i\n", i);
		sleep(1);
	}

	printf("Thread A stops...\n");

	return NULL;
}

void *thread_funcB(void* arg){

	printf("Thread B starts...\n");

	for (int i = 0; i < 10; ++i){
		printf("Th B %i\n", i);
		sleep(1);
	}

	printf("Thread B stops...\n");
	
	return NULL;
}