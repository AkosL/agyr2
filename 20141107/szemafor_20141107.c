/* szemafor_20141107 
	bináris szemaforok

	fordításhoz külön be kell húzni a pthread-et: -lpthread
	
	{
	"cmd" : ["gcc", "$file_name", "-o", "${file_base_name}", "-std=c99", "-lpthread", "-lm", "-Wall"],
	"selector" : "source.c",
	"shell":false,
	"working_dir" : "$file_path"
	}
*/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

#define WORK_AREA_SIZE 1024

void *thread_funcA(void *arg);
char work_area[WORK_AREA_SIZE];
sem_t bin_sem;

int main(void) {
	int res;
	pthread_t myThreadA;
	void* threadResult;

	res = sem_init(&bin_sem, 0, 0);
	

//indíjjuk el a threadeket
	res = pthread_create(&myThreadA, NULL, thread_funcA, NULL);
	printf("írjá be vlamit, és csapj egy entert!\n");


	while(strncmp("end", work_area, 3)  != 0){
		fgets(work_area, WORK_AREA_SIZE, stdin);
		sem_post(&bin_sem);
	}

	printf("várok a másikra....\n");

	res = pthread_join(myThreadA, &threadResult);

	sem_destroy(&bin_sem);



	printf("Main thread stops\n");
	return 0;
}

void *thread_funcA(void* arg){
	sleep(1);

	printf("Thread A starts...\n");

	sem_wait(&bin_sem);

	while(strncmp("end", work_area, 3) != 0){
		printf("you input %d characters\n", strlen(work_area));
		work_area[0] = 0;
		sem_wait(&bin_sem);
	}

	printf("Thread A stops...\n");

	return NULL;
}