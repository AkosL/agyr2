/* thread_20141107 
	fordításhoz külön be kell húzni a pthread-et: -lpthread
	
	{
	"cmd" : ["gcc", "$file_name", "-o", "${file_base_name}", "-std=c99", "-lpthread", "-lm", "-Wall"],
	"selector" : "source.c",
	"shell":false,
	"working_dir" : "$file_path"
	}
*/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

void *thread_func(void *arg);

int main(void) {

	pthread_t myThread;

	if(pthread_create(&myThread, NULL, thread_func, NULL)){
		fprintf(stderr, "BUGZIK!\n");
		exit(1);
	}

	sleep(5);

	printf("Main thread stops\n");//azt írta, h ez itt csúnya....
	return 0;
}

void *thread_func(void* arg){

	printf("Thread starts...\n");

	for (int i = 0; i < 10; ++i){
		printf("Th1 %i\n", i);
		sleep(1);
	}

	printf("Thread stops...\n");
}