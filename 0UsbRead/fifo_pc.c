#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>

/* fifóval usb soros port bizerbálás
*/

int main(void){
	FILE* fp;
	int hnd;
	unsigned f;
	char buff[80];
	
	//konfigoljuk be a soros portot
	system("stty -F /dev/ttyUSB0 19200 cs8");
	
	fp = fopen("/dev/ttyUSB0", "r+");
	if(fp==NULL){
	  printf("Nyekk");
	  return 0;
	}
	
	//szerezzük meg a USB port hozzáférést alacsonyszinten
	hnd = fileno(fp);
	f = fcntl(hnd, F_GETFL);
	//és legyen nonblocking
	fcntl(hnd, F_SETFL, f|0_NONBLOCK);
	
	while(1){
	  if(0<fscanf(fp, "%s", buff)){
	    printf("%s\n", buff);
	  }
	  usleep(10000);
	  printf(".");
	}
	
	//sima egyszeri kiírás + visszaolvasás
	fprintf(fp, "Hello\n");
	fscanf(fp, "%s", buff);
	
	printf("%s\n", buff);
	
	fclose(fp);
	return 0;	
}
